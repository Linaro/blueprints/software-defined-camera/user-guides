# Setup target device to enable non-root sudo user

* Ensure you have a non-"root" user setup with root (sudo) privilidges.\
  
  Assuming you are logged in as "root" user, run the following commands: 
  ```
  adduser <username>
  ```
  
  Enter a password for the user
  ```
  Enter new UNIX password:
  Retype new UNIX password:
  passwd: password updated successfully
  ```

  Optionally add user details: 
  ```
  Changing the user information for sammy
  Enter the new value, or press ENTER for the default
    Full Name []:
    Room Number []:
    Work Phone []:
    Home Phone []:
    Other []:
    Is the information correct? [Y/n]
    ```

    Add new user to sudoer group
    ```
    usermod -aG sudo <username>
    ```

    Test the new user account for root priviledges: 
    ```
    su - <username>
    ls -la /root
    ```


* Ensure you have followed the Post-installation steps for docker engine \
  (https://docs.docker.com/engine/install/linux-postinstall/)
