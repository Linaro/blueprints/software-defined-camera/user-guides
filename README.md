# About Software Defined Camera 

This reference implementation of a Software Defined Camera solution required the following components: 

1. **Reference Plarform** (SystemReady-IR Certified hardware platform )
2. **Reference Software Stack** (SystemReady compatible Linux distribution)
3. **Reference Cloud-Native application** (madeup of containerised microservices)

The diagram below shows an overview how the Hardware-Software integration is done: 

![Software Defined Camera Reference Implementation](./assets/images/sdc-reference-implementation.png)



### Reference Software Stack
This is an open source SOAFEE (soafee.io) compliant Linux OS that can be deployed on any SystemReady-IR Certified platform

### Reference Cloud-Native Application
This is an example AI enabled application built using microservice based architecture and can be deveveloped and deployed in multiple configurations. 

#### Deployment configurations

1. **Standalone**: Just the core microservices required to run the application locally (contains a pre-trained model)
2. **CSP-integration**: Adds an addtitional microservice that allows deployment to connect to a supported cloud service provider 
3. **Remote-split**: Running only the essential microservices locally and offloading compute heavy microservices like ML-inference to run on remotely (eg. Cloud, edge-server etc.)  

#### Development components: 
1. **Application Microservices repos**: These are made up of 3 repositories (cn-video-capture, cn-application, cn-inference)
2. **CSP microservice repos**: These contain the repositories for the 2 supported CSPs (Alibaba and AWS) 
3. **ML Model repos**: These repos allow you to build containers that train and compile the model 


## Software Defined Camera Repositores Structure: 

	Software Defined Camera (Group)

    	- User Guides (repo) 
    	- Manifests (repo) 
    
    	- Reference Use-Cases (Group)
        	- Traffic Monitoring (Group)
          	  - Application Microservices (Group)
             	   - CloudNative Application (repo)
             	   - CloudNative Inference (repo)
             	   - CloudNative Video Capture (repo)
            	- Cloud Service Provide Integration (Group)
             	   - Amazon Web Services (repo)
             	   - Alibaba Cloud (repo)
            	- ML Model (Group)
             	   - Training (repo)
             	   - Compilation (repo)
        
  
    	- Reference SW stack (Group)
    	      - SDC Yocto (repo)
        


## User guides: 

1. [Getting Started Guide](./getting-started-guide.md): Basic user guide for SW stack and use-case bring-up
2. [AWS Account Setup](./aws-account-setup.md): User guide to setting up an account for AWS
3. [Deploy use-case with AWS](./deploy-use-case-with-aws.md): Deploy example use-case with AWS integration
4. [Software Defined Camera CI](./software-defined-camera_ci.md): Linaro's CI implementation around SDC
5. [Setup target for non-root sudo user](./setup-non-root-sudo-user.md): Appendix guide to setup a non-root user with sudo priviledges

