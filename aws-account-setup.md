# AWS IAM roles and users setup guide
  
This guide will walk you through the account creation process and explain the purpose of the IAM credentials.

## Create AWS Account
This process can be skipped if you already have an AWS account that fulfills the following requirements:
1. Ability to create and manage IAM users & groups
2. Ability to create and manage IAM roles
3. Ability to grant access for AWS services to IAM users/roles

If you don't already have an AWS account, you will need to sign up for one.

### Pre-requisites for registration:
1. Email address to register
2. Active credit card that can be charged and verified during registration
3. Contact phone/mobile number

To register goto: https://portal.aws.amazon.com/


## Create IAM role and user

### IAM role:
This allows a user to create a profile for use with services on AWS. You can select the services you want to make available for your use-case.

1. Goto AWS Dashboard/Console
2. Search for and select IAM (Identity and Access Management) service
3. On the left-hand side pane select Access management >> Roles
4. Click on the Creat role button on the right hand side of the webpage
5. Select trusted entity: 
   - Trusted entity type: AWS service
   - Use Case: Other AWS services 
   - Search: SageMaker >> Select: SageMaker - Execution
6. Click Next
7. Pick a name for the IAM role and optionally add a description
8. Click on "Create role" button
9. Once the role is created selec the role
10. Find and click the "Add permissions" button.
11. Search for the following policies and add them one by one:
    - AmazonEC2FullAccess: Provides full access to Amazon EC2 via the AWS Management Console.
    - IAMFullAccess: Provides full access to IAM via the AWS Management Console.
    - AmazonS3FullAccess: Provides full access to all buckets via the AWS Management Console.
    - AWSGreengrassFullAccess: This policy gives full access to the AWS Greengrass configuration, management and deployment actions.
    - AmazonSageMakerFullAccess: Provides full access to Amazon SageMaker via the AWS Management Console and SDK.

### IAM user: 
This allows an end-point to connect to AWS and request services from AWS via an IAM role.

1. Goto AWS Dashboard/Console
2. Search for and select IAM (Identity and Access Management) service
3. On the left-hand side pane select Access management >> Users
4. Click on the Add user button on the right hand side of the webpage
5. Pick a name for the IAM user 
6. Select AWS credential type: [] Access key - Programmatic access
7. Click on "Next: Permissions"
8. Set permissions: Select Attach existing policies directly
9. Search for and select the following Amazon Services:  
   - AmazonEC2ContainerRegistryFullAccess
   - AmazonEC2FullAccess
   - AmazonElasticContainerRegistryPublicFullAccess
   - AmazonS3FullAccess
   - AmazonSageMakerFullAccess
   - AWSGreengrassFullAccess
   - AWSIoTFullAccess
   - CloudWatchFullAccess	
   - IAMFullAccess

10. Click on "Next:Tags" button
11. Optionally add tags for the IAM user (skip: not needed)
12. Click on "Next:Review" button
13. Review details to make sure all the required Amazon services are listed
14. Click on "Creat user"
15. Note down the following for use later: 
    - Access key ID (displayed)
    - Secret access key (hidden: click "Show")
16. Click on "Close"

Once the IAM role and user has been setup you proceed to bulding and deploying the use-case example application with AWS integration.

