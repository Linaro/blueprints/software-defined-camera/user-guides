# AWS EC2 Quick Setup Guide

## Pre-requisites: 
1. AWS user account with Root priviledges
2. AWS VPC (Virtual Private Cloud) instance with public Subnet, RouteTable and Internet Gateway


This service is required to run inference in the cloud
(Skip this if you only plan to run inference on the edge)

1. Navigate to AWS EC2 concole https://console.aws.amazon.com/ec2 
2. Make sure the right region is selected
3. Click on the "Instances" link on the left hand side tab 
4. Click on "Launch Instance" button
5. Create a new instance with the following configuration: 
* Instance Distro: `Ubuntu`
* Amazon Machine Image (AMI): Ubuntu Server 18.04 LTS (HVM) SSD Volume type
* Architecture: 64-bit (Arm) 
* Instance type: `t4g.xlarge`
* Key Pair (login): Create new key Pair
    * Key pair name: `<Enter name>`
    * Key pair type: RSA
    * Private key file format: `.pem`
    * Click on "Create key pair"
    * This will download the key pair file, it will be required to ssh into the EC2 instance. Keep it safe. 
    * Select key pair name from drop down menu. 
* Network Settings: Click on Edit
    * Scroll down and click on "Add security group rule"
    * For the new security rule use the following configuration: 
        * Type: All traffic
        * Source type: Anywhere
* Configure storage: `1x 18 GiB gp2 Root volume` 
* Click: Launch instance  

A new EC2 instance should now get created and start running. The status of the EC2 instance can be monitored on the EC2 dashboard
To check connectivity to the EC2 instance do the following: 
* Ping the EC2 instance: `ping <ec2-instance-public-ip>`
* Select the EC2 instance on the dashboard and find it's "Public IPv4" 
* Find the downloaded key pair .pem file and change the permission using: `chmod 400 <key-pair-name>.pem` 
* SSH into the EC2 instance using the commnad: `ssh -i <path to key pair .pem file> ubuntu@<ec2-instance-public-ip>`

### Troubleshooting: 
* If the ping command times out it means the security configuration wasn't correctly setup
    * Ensure the All traffic (on all ports and all protocols) rule is added to the security group being used
* If ssh into EC2 instance doesnt work: 
    * Check the IP address, make sure it's the correct Public IP for the EC2 instance  
    * Check the path for the *.pem file
    * Check if the permissions are correctly set on the *.pem file

