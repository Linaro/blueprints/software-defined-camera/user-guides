# Software Defined Camera Getting Started Guide

This guide will walk the reader through the steps requred to build and deploy: 
1. Reference SW stack: A Yocto-Based SystemReady Reference Linux OS capable of running a reference application
2. Reference use-case: Default configuration of Reference Traffic monitoring cloud-native application


## Prerequisites

### Target HW platform for Reference Software Stack
The HW plaform must be an (arm-based) SystemReady-IR certified platform that is able to boot from a flashable storage medium.\
**CPUs**: min. 1 (preferred 4 or more) \
**RAM**: min. 1GB (preferred 4GB or more) \
**Storage**: min. 16GB (preferred 32GB or more) \
**Connectivity**: Ethernet & UART

	Note: On the Rockchip platforms the Baudrates for UART connection changes during boot
    * uboot: 1500000 
    * linux (getty): 115200 


### Build/Host Machine 

**OS**: Linux (Ubuntu 20.04 or later) \
**CPUs**: min. 4 (preferred 16 or more) \
**RAM**: min 16GB (preferred 64 GB or more) \
**Storage**: min. 100G (preferred 512GB or more) \
**Connection**: Serial & Ethernet connection to Target HW platform


### UI display (can be the same as Build/Host machine)
You will need a machine to display the Web-UI website hosted by the Target HW platform
This machine should be able to render and display a Web-browser eg

### Cloud Service Provider account/credentials
Currently only 1 cloud-service provider is supported: \
* AWS : You will need root user credentials if you want to deploy with AWS integration.

    Note: Support for AliCloud integration has been dropped due to recent changes in some of its services 


### Reference use-case

    Note: It is not mandatory to use the Reference SW stack for running the reference use-case. 

The reference use-case requires an operating system to be running on the Target HW platfrom that meets the following requirements: \
    1. Linux Based OS\
    2. Running systemd \
    3. Container runtime: Docker\
    4. Container Orchestration: k3s\
    5. Other SW packages: git, python, python3, repo, wget


Other recommended Linux Distros: \
    * Linaro Trusted Reference Stack (preferred alternative) \
    * Ubuntu/Fedora/OpenSuse etc. for arm64


## Building and deploying reference Linux OS
Refer to [Reference SW Stack Readme](https://gitlab.com/Linaro/blueprints/software-defined-camera/reference-sw-stack/sdc-yocto/-/blob/main/README.md)


## Building and deploying default reference use-case 
There are multiple ways you can deploy the reference use-cases

The easiest way to deploy the cloud-native application is to deploy it as standalone - without CSP (Cloud Service Provider) integration. 
This does not require an account/credentials with CSP 

Other alternatives include: \
    1. Deploy with CSP integration \
    2. Deploy across edge-cloud interface


It is mandatory to have a CSP account for the above alternatives. 



### Deploying standalone use-case


![Standalone Application Overview](/assets/images/sdc-deploy-standalone.png)

#### Setup Target device: 
You will need some means of interacting with the Target Platform. Depending on the capabilities of the device this can be done using one of the following ways: 
1. Serial Port Connection (Connected to a Build/Host machine. Connection parameters vary depending on the board vendor)
2. Ethernet (SSH. For this this the target platform should be connected to the same network as the host and the IP address must be known)
3. HID device and display (If the target plaform & OS supports display and desktop env) - This is not available in the Reference SW stack. 

Once you have the Reference SW Stack (Linux OS) mentioned above running on your target platform and are able to connect to a terminal/shell prompt, run the following commands to check if the necessary components were configured and installed correctly

``` 
sudo docker run hello-world 
```

The reference use-case is a traffic monitoring application implemented as 3 containerised microservices. Each microservice has it's own git repo. 

Now you will need to follow the following steps: 

1. Clone the SDC use-case repositories 
2. Build the 3 contianers (microservices)
3. Run the 3 docker contianers in a fixed sequence




#### Clone the SDC traffic monitoring use-case repos
On the target platform run the following commands: 
```
mkdir sdc
cd sdc
repo init -u https://gitlab.com/Linaro/blueprints/software-defined-camera/manifest.git -b Release_0.1 -m deploy-app-standalone.xml
repo sync
cd reference-use-cases/traffic-monitoring/application-microservices
```

### Build the 3 containers
The 3 microservices are: 
    1. Video-capture container 
    2. Inference container
    3. Application container 

#### Build the video-capture container: 
Note: To use this contianer you must use one of the 2 following video sources: \
    *  A `v4l2` USB webcam /MIPI camera as `/dev/video0` device\
    *  A `video.mp4` file copied into the folder containing this repo. 

``` 
cd cn-video-capture
# Example video file from: https://www.pexels.com/video/cars-on-the-road-2034115/ 
wget "https://www.pexels.com/download/video/2034115/?fps=29.97\&h=360\&w=640" -O video.mp4
docker build -t cn-video-capture:latest .
cd .. 
```

#### Build the Inference container:
```
cd cn-inference
docker build -t cn-inference:latest .  
cd ..
```


#### Build the Application container: 
```
cd cn-application
docker build -t cn-application:latest .  
cd ..
```


### Run containers

The containers need to be run in a specific sequence. 
The video capture microservice (container) and the inference microservice (container) need to be up and running before the application microserivce (container) is started. 

1. Running the video-capture microservice: 
    By default the video-capture container expects the video.mp4 file to be avialable for streaming video into the application
    Ensure a video.mp4 file is copied into the cn-video-capture folder before the cn-video-capture container is built. 
    ```
    docker run -it --rm --name video_stream --network=host cn-video-capture:latest /mediamtx mp4.yml
    # Press Ctrl P + Q to move the container to run in background 
    ```

<details>
    <summary> OPTIONAL: </summary>  
        
    To run the use-case using a USB webcam/MIPI camera 
    ```
    # First List the dev/video devices available on your targer platform
    ls -l /dev/video*
    ```

    Identify the /dev/videoX device your input webcam/camera device
    Replace the 'X' in the `--device=/dev/videoX:/dev/video0` mapping below with number that corresponds to your camera device. 
    ```
    docker run -it --rm --name video_stream --device=/dev/video2:/dev/video0 --network=host cn-video-capture:latest /mediamtx usb_webcam.yml
    # Press Ctrl P + Q to move the container to run in background 
    ```
</details>

2. Running the Inference microserivce: 
    ```
    docker run -it --rm --name inference --network=host cn-inference:latest -p 8080 -m tiny_yolov3 -c <number-of-CPUs>
    # Press Ctrl P + Q to move the container to run in background 
    ```

    Note: Due to this being the most compute intensive microservice you can assign or restrict the CPUs allocated to this container. For more details refer to: [CloudNative Inference Readme](https://gitlab.com/Linaro/blueprints/software-defined-camera/reference-use-cases/traffic-monitoring/application-microservices/cn-inference/README.md) 

3. Running the Application microservice: 
    First ensure the other containers are up and running on the target device: 
    ``` 
    docker ps
    ```
    You should see a log as follows: 
    ```
    c41972799035   cn-inference:latest       "python3 inference.p"   33 seconds ago   Up 32 seconds             inference
    12a13529d2c0   cn-video-capture:latest   "/mediamtx"              2 minutes ago    Up 2 minutes             video_stream

    ```
    If both of the above containers are running proplerly, start the cn-application container
    ```
    docker run -it --rm --name application --network=host cn-application:latest -p 8080 -m tiny_yolov3 -r rtsp://localhost:8554/cam 
    # Press Ctrl P + Q to move the container to run in background 
    ```

<details>
    <summary> Using WEBCAM/MIPI camera </summary>  
        
    ```
    If both of the above containers are running proplerly, start the cn-application container
    ```
    docker run -it --rm --name application --network=host cn-application:latest -p 8080 -m tiny_yolov3 -r rtsp://localhost:8554/cam
    # Press Ctrl P + Q to move the container to run in background 
    ```
</details>

### Launch Web-UI 

Now that all the microservices are up and running to trigger the application pipeline you need the launch the Web-UI by navigating to: `IP-ADDR-TARGET-DEVICE:5000`

    * Open a browser window on a Computer connected to the same network as the target HW platform
    * Navigate to: <ip-addr-device>:5000 (eg. `192.168.0.11:5000`)
    * You should see a web-page that looks like the image shown below: 


![Web UI example](/assets/images/web-ui-example.png)  
    