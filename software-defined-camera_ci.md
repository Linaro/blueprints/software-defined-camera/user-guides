# Software Defined Camera CI implementation

Linaro has been working on taking this example use-case and creating a reference CI implementation. 

This CI implementation can be found here: https://gitlab.com/jforissier/smart-camera-v2/ 

This CI framework builds on the following Linaro Projects: 
1.  Trusted Substrate (meta-ts): To provide SystemReady compliant firmware implementations for supported Arm Platforms with Security features
2.  Trusted Reference Stack (TRS): To provide a SystemReady reference SW stack which along with providing a cloud-native Linux Distro provides the interfaces to use the security features implemented by Trusted Substrate. 


Follow the instructions documented in the Readme.md file in the above repository to deploy and test the use-case CI on your supported SystemReady platform.

    Note: The above CI framework has only beed tested on the following platforms: 
        1. Radxa RockPi4 (main brnach  and rel 0.3 tag)
        2. Xilinx KV260 (main branchand rel 0.3 tag)
        3. NXP i.mx8mplus-evk (main branch only)
