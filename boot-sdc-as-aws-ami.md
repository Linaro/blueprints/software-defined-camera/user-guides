# How to Boot the SDC SW stack as an AWS AMI

If you don't have a SystemReady certified board available is possible to boot the Reference Software Stack on an EC2 instance as an Amazon Machine Image (AMI)


## Prequsites: 
1. Build/Host machine
2. AWS account with necessary priviledges



## Building the SystemReady image for Graviton Instances

To do this you will need to rebuild Reference Software stack with additional recipies required by AWS EC2 instances to boot SR images. These packages are included in the graviton-ami.yml kas file. 


```
kas build kas_configs/csp/aws:kas_configs/sdc.yml:kas_configs/graviton-ami.yml 
```


## Uploading Image to EC2 as AMI 

Now there are multiple ways this can be done. 
1. Using aws-cli on a build machine
2. Build on and deploy from the same EC2 instance 
3. Build on local host and upload to EC2 instance 

 
> **_NOTE:_**  Your experience may vary based on the permissions and policies associated with your AWS account. Especially if you are using a Organization Managed account.



### 1. Using aws-cli:
If you already have AWS tools like awscli installed and your AWS account (with all the necessary permissions) configured on the host/build machine this is the most direct way of creating an EC2 Graviton AMI. 
a. Build SDC software stack as documented <here>
b. Follow the instructions privded here: <insert-link-here> 

Note: This build machine can also be an EC2 instance


### 2. Build on and deploy from the same EC2 instance
This method also relies on having an AWS account but only requires the set of permissions that allow you to create and deploy AWS EC2 instances with externel internet access. 
(This is a smaller set of permissions required for the IAM user when compared to #1)


For this method you will need 2 EC2 instances: 
1. For building the SDC Reference SW stack image (sdc-builder)
2. To boot the SDC reference stack built by the 1st one (sdc-device)


![Creating SDC AMI using EC2 instances](/assets/images/create-ami-ec2.png)


Follow the steps below: 
1. Create an SDC-Builder instance with 2 stoarge drives attached
2. Follow the instructions to build SDC reference stack on SDC-Builder
3. Write the SDC image onto the 2nd storage drive
4. Create a snapshot of the 2nd storage drive in AWS EC2 console
5. Use the Created Snapshot to create an AMU in AWS EC2 console
6. Create a new SDC-DEVICE instance (Select Custom AMI option and choose the AMI created above)
7. Spin up the new SDC-DEVICE instance and resize the parition if required (delete swap if required)

 



### 3. Build on local host and upload to EC2
This is method is similar to that described above 

![Creating SDC AMI using local PC and EC2 instances](/assets/images/create-ami-pc-ec2.png)


1. Follow the steps to build SDC refernce stack on a local build machine
2. Spin up an EC2 instance with 2 stoarge drives attached
3. Copy (eg. `scp`) the built SDC image onto the EC2 instance create in #2
4. Write (dd) the SDC image onto the 2nd Storage drive
5. Create a snapshot of the 2nd storage drive in AWS EC2 console
6. Use the Created Snapshot to create an AMU in AWS EC2 console
7. Create a new SDC-DEVICE instance (Select Custom AMI option and choose the AMI created above)
8. Spin up the new SDC-DEVICE instance and resize the parition if required (delete swap if required)
 



