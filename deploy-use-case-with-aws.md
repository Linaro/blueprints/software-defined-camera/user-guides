# Deploy use-case application with Amazon Web Services


![SDC workflow example with AWS] (/assets/images/sdc-workflow-example.jpg)

The above diagram shows how a cloud-native application may be deployed with Cloud-Service provider integration: 


    Note: This user-guide assumes the user is already familiar with standalone deployment for the SDC cloud-native 
          use-case explained in the "Getting_Started_Guide.md"


It is optional to use AWS EC2 to build and commit the docker images to an AWS Elastic Contatiner Registry (ECR).


## Creating/Setting-up AWS account and credentials
Refer to the [AWS Account Setup Guide](./aws-account-setup.md)  


## Setting up EC2 instance (Optional)
Refer to the [AWS EC2 instance setup](./aws-ec2-instance-setup.md)


## Deploy Greengrass and Cloud containers
For more details on this refer to Readme in the [CSP Integration repository](https://gitlab.com/Linaro/blueprints/software-defined-camera/reference-use-cases/traffic-monitoring/csp-integration/csp-amazon-web-services/-/tree/Release_0x/)

    Note: 
    1. Ensure you bring-up the Greengrass container before you bring-up the Cloud Container in the CSP integration repo. 
    2. The onboot and kubernetes features are optional and are not expected to be running for this guide. 


Once you've completed the bring up of the Greengrass and Cloud containers as well as setting up of Cloudwatch service on AWS you will need to launch the Application microservices. 


### Overview of container deployments: 

Assuming all the steps to build the containers and install the certificates have been followed in the Readme/instructions above.
The commands below give an overview of all the containers you need to run for the use-case to run locally on an edge device and publish \
inference metrics to the cloud (the AWS cloudwatch service to be precise)

```
# First time only to extract AWS certificates from Greengrass container
docker run --init -it --name aws-iot-greengrass -v <AWS_cred_file_path>:/root/.aws/:ro --env-file <.env_file_path> -p 8883 cn-greengrass:latest


# Assuming CSP container has been built with AWS certificates and Cloudwatch is setup on AWS console
docker run -it --rm --name csp_intg --network=host cn-csp-intg-aws:latest -p 8080 -cw_i 30 
docker run -it --rm --name video_stream --network=host cn-video-capture:latest /mediamtx mp4.yml
docker run -it --rm --name inference --network=host cn-inference:latest -p 8080 -m tiny_yolov3 -c 4
docker run -it --rm --name application --network=host cn-application:latest -p 8080 -m tiny_yolov3 -r rtsp://localhost:8554/cam -cloud aws -csp 127.0.0.1:8080

```

    Note: The application container needs addtitional arguments to know it is being run with CSP integration 